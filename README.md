# Fuseki Docker Image

This repository is a wrapper around the Jena Fuseki image `stain/jena-fuseki`.

The resulting image contains Fuseki service and dataset configurations for usage in the TSG Metadata Broker.